<?php

namespace Tests\Feature;

use App\Services\CustomerService;
use Database\Seeders\CustomerCategorySeeder;
use Database\Seeders\CustomerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Pagination\LengthAwarePaginator;
use Tests\TestCase;

class CustomerServiceTest extends TestCase
{
    use RefreshDatabase;

    private CustomerService $customerService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->customerService = $this->app->make(CustomerService::class);
    }

    public function testCreateCustomer()
    {
        $this->assertIsInt($this->customerService->create([
            'name' => 'Fatah',
            'number' => '0123456789',
            'address' => 'Taman Cihanjuang',
            'gender' => 1,
            'dob' => '1996-12-03',
        ]));
    }

    public function testUpdateCustomer()
    {
        $this->seed(CustomerSeeder::class);

        $this->assertTrue($this->customerService->update(1, [
            'name' => 'Fatah 1',
            'number' => '01234561',
            'address' => 'Taman Cihanjuang 2',
            'gender' => 1,
            'dob' => '1996-12-03',
        ]));
    }

    public function testDeleteCustomer()
    {
        $this->seed(CustomerSeeder::class);

        $this->assertTrue($this->customerService->delete(1));
    }

    public function testGetAllCustomerWithPaging()
    {
        $this->seed(CustomerSeeder::class);
        $this->assertInstanceOf(LengthAwarePaginator::class, $this->customerService->findAllByPage(10));
    }

    public function testGetCustomerById()
    {
        $this->seed([CustomerSeeder::class]);

        $customer = $this->customerService->findById(1);

        $this->assertIsObject($customer);
        $this->assertSame('Fatah', $customer->name);
    }

    public function testCreateCustomerCategory()
    {
        $this->assertIsInt($this->customerService->categoryCreate([
            'name' => 'Umum',
        ]));
    }

    public function testUpdateCustomerCategory()
    {
        $this->seed(CustomerCategorySeeder::class);

        $this->assertTrue($this->customerService->categoryUpdate(1, [
            'name' => 'Umum 1',
        ]));
    }

    public function testDeleteCustomerCategory()
    {
        $this->seed(CustomerCategorySeeder::class);

        $this->assertTrue($this->customerService->categoryDelete(1));
    }

    public function testGetAllCustomerCategoryWithPaging()
    {
        $this->seed(CustomerCategorySeeder::class);
        $this->assertInstanceOf(LengthAwarePaginator::class, $this->customerService->categoryFindAllByPage(10));
    }

    public function testGetCustomerCategoryById()
    {
        $this->seed([CustomerCategorySeeder::class]);

        $customerCategory = $this->customerService->categoryFindById(1);

        $this->assertIsObject($customerCategory);
        $this->assertSame('Umum', $customerCategory->name);
    }

    public function testCustomerCategoryFindAll()
    {
        $this->seed(CustomerCategorySeeder::class);

        $this->assertIsArray($this->customerService->categoryFindAll());
    }
}
