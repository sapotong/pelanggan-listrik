<?php

namespace Tests\Feature;

use App\Services\PaymentService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class PaymentServiceTest extends TestCase
{
    private PaymentService $paymentService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->paymentService = $this->app->make(PaymentService::class);
    }

    public function testPaymentTime()
    {
        $this->assertTrue($this->paymentService->createPayTime('2022-11'));
        
        DB::table('payments_times')->truncate();        
    }

    public function testPay()
    {
        $this->assertTrue($this->paymentService->pay([
            ['customer_id' => 1, 'time_id' => 1, 'status' => 1],
            ['customer_id' => 2, 'time_id' => 1, 'status' => 0],
        ]));

        DB::table('payments')->truncate();
    }
}
