<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PaymentControllerTest extends TestCase
{
    public function testPaymentPage()
    {
        $response = $this
            ->withSession([
                'username' => 'fatah'
            ])
            ->get('/payment');

        $response->assertStatus(200)->assertSeeText('Pembayaran');
    }

    public function testPayTimeCreatePage()
    {
        $this
            ->withSession(['username' => 'fatah'])
            ->get('/payment/create')
            ->assertStatus(200)
            ->assertSeeText('Tambah Pembayaran');
    }

    public function testPayTimeCreatePost()
    {
        $this
            ->withSession(['username' => 'fatah'])
            ->post('/payment/create', [
                'month' => '2022-11'
            ])
            ->assertRedirect('/payment')
            ->assertSessionHas('success');
    }
}
