<?php

namespace Tests\Feature;

use App\Models\RegisterRequest;
use App\Services\UserService;
use Database\Seeders\UserSeeder;
use Exception;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\TryCatch;
use Tests\TestCase;

class UserServiceTest extends TestCase
{
    use RefreshDatabase;

    private UserService $userService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->userService = $this->app->make(UserService::class);
    }

    public function testLoginSuccess()
    {
        $this->seed(UserSeeder::class);

        self::assertTrue($this->userService->login('fatah', 'kalaider'));
    }

    public function testLoginUserNotFound()
    {
        self::assertFalse($this->userService->login('salah', 'salah'));
    }

    public function testLoginWrongPassword()
    {
        $this->seed(UserSeeder::class);

        self::assertFalse($this->userService->login('fatah', 'salah'));
    }

    public function testRegisterSuccess()
    {
        $registerData = [];
        $registerData['name'] = "Fatah 2";
        $registerData['username'] = "fatah2";
        $registerData['email'] = "fatah2@fatah.id";
        $registerData['password'] = "fatah2";
        
        self::assertTrue($this->userService->register($registerData));
    }
    
    public function testRegisterUsernameExist()
    {
        $this->seed(UserSeeder::class);
        
        $registerData = [];
        $registerData['name'] = "Sudah ada";
        $registerData['username'] = "fatah";
        $registerData['email'] = "sudah1@ada.com";
        $registerData['password'] = "sudahada";
        
        try {
            $this->userService->register($registerData);
        } catch (Exception $e) {
            self::assertSame('Username sudah terdaftar', $e->getMessage());
        }
    }
    
    public function testRegisterEmailExist()
    {
        $this->seed(UserSeeder::class);

        $registerData = [];
        $registerData['name'] = "Sudah ada";
        $registerData['username'] = "sudah_ada2";
        $registerData['email'] = "fatah@kawatama.com";
        $registerData['password'] = "sudahada";
        
        try {
            $this->userService->register($registerData);
        } catch (Exception $e) {
            self::assertSame('Email sudah terdaftar', $e->getMessage());
        }
    }
}
