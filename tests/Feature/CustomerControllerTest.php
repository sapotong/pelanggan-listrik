<?php

namespace Tests\Feature;

use Database\Seeders\CustomerSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CustomerControllerTest extends TestCase
{
    use RefreshDatabase;
    
    public function testCustomerPage()
    {
        $this->withSession(['username' => 'fatah'])
            ->get('/customer')
            ->assertSeeText('Pelanggan');
    }

    public function testCustomerCreatePost()
    {
        $this->withSession(['username' => 'fatah'])
            ->post('/customer/create', [
                'name' => 'Fatah',
                'number' => '123456789',
                'gender' => 1,
                'dob' => '1996-06-01',
                'address' => 'Cihanjuang'
            ])
            ->assertRedirect('/customer')
            ->assertSessionHas('success');
    }
    
    public function testCustomerEditPost()
    {
        $this->seed(CustomerSeeder::class);

        $this->withSession(['username' => 'fatah'])
            ->post('/customer/1/edit', [
                'name' => 'Fatah 1',
                'number' => '123456789',
                'gender' => 1,
                'dob' => '1996-06-01',
                'address' => 'Cihanjuang'
            ])
            ->assertRedirect('/customer')
            ->assertSessionHas('success');
    }
    
    public function testCustomerDelete()
    {
        $this->seed(CustomerSeeder::class);

        $this->withSession(['username' => 'fatah'])
            ->post('/customer/1/delete')
            ->assertRedirect('/customer')
            ->assertSessionHas('success');
    }
}
