@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">{{$title}}</h1>
    <a href="/payment/create" class="btn btn-primary">Tambah Pembayaran</a>
</div>

@if(request()->session()->has('success'))
    <div class="alert alert-success">{{request()->session()->get('success')}}</div>
@endif
@if(request()->session()->has('error'))
    <div class="alert alert-danger">{{request()->session()->get('error')}}</div>
@endif

<table class="table table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Bulan</th>
            <th>&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($pay_times as $item)
            <tr>
                <td>{{$item->id}}</td>
                <td>{{$item->month}}</td>
                <td>
                    <a href="{{ url("/payment/{$item->id}/edit") }}" class="btn btn-primary btn-sm">Edit</a>
                    <a href="{{ url("/payment/{$item->id}/pay") }}" class="btn btn-info btn-sm">Pembayaran</a>
                    <form class="d-inline" action="{{ url("/payment/{$item->id}/delete") }}" method="post" onSubmit="return confirm('Apakah anda yakin menghapus pembayaran ini?')">
                        @csrf
                        <button class="btn btn-danger btn-sm" type="submit">Hapus</button>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="7">Pelanggan belum tersedia</td>
            </tr>
        @endforelse
    </tbody>
</table>

{{$pay_times->links()}}

@endsection
