@extends('layouts.app')

@section('title', $title)

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">{{$title}}</h1>
</div>

@if(request()->session()->has('success'))
<div class="alert alert-success">{{request()->session()->get('success')}}</div>
@endif
@if(request()->session()->has('error'))
<div class="alert alert-danger">{{request()->session()->get('error')}}</div>
@endif

<form action="{{ url("/payment/{$payment->id}/pay") }}" method="post">
    @csrf
    
    <div class="row justify-content-center">
        <div class="col-lg-8">
            @if(isset($error))
                <div class="alert alert-danger">{{$error}}</div>
            @endif
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td>Bulan</td>
                        <td>{{$payment->month}}</td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Nomor</th>
                        <th>Pelanggan</th>
                        <th colspan="2">Bayar</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($customers as $customer)
                    <tr>
                        <td>{{$customer->number}}</td>
                        <td>
                            {{$customer->name}}
                            <input type="hidden" name="pays[{{$customer->id}}][pay_id]" value="{{$customer->pay_id}}">
                        </td>
                        <td>
                            <div class="form-check">
                                <input type="radio" name="pays[{{$customer->id}}][status]" id="donePay{{$customer->id}}" value="1" @checked($customer->status == 1)>
                                <label class="form-check-label" for="donePay{{$customer->id}}">
                                    Sudah
                                </label>
                            </div>
                        </td>
                        <td>
                            <div class="form-check">
                                <input type="radio" name="pays[{{$customer->id}}][status]" id="notYetPay{{$customer->id}}" value="0" @checked($customer->status == 0)>
                                <label class="form-check-label" for="notYetPay{{$customer->id}}">
                                    Belum
                                </label>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="7">Belum ada data pelanggan</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>

            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </div>

</form>



@endsection