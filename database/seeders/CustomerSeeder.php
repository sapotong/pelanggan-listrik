<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->delete();
        DB::table('customers')->insert([
            'id' => 1,
            'name' => 'Fatah',
            'number' => '012345678',
            'gender' => 1,
            'dob' => '1996-12-03',
            'address' => 'Taman Cihanjuang',
            'category_id' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        foreach(range(2, 20) as $id) {
            DB::table('customers')->insert([
                'name' => fake('id_ID')->name(),
                'number' => fake('id_ID')->nik(),
                'gender' => fake('id_ID')->numberBetween(1, 2),
                'dob' => fake('id_ID')->date('Y-m-d', '2007-01-01'),
                'address' => fake('id_ID')->address(),
                'category_id' => 1,
                'created_at' => date('Y-m-d H:i:s')
            ]); 
        }
    }
}
