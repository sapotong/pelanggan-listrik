<?php

use App\Http\Controllers\HomeController;
use App\Http\Middleware\OnlyGuestMiddleware;
use App\Http\Middleware\OnlyMemberMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->middleware([OnlyMemberMiddleware::class]);

Route::controller(\App\Http\Controllers\UserController::class)->group(function() {
    Route::get('/login', 'login')->middleware([OnlyGuestMiddleware::class]);
    Route::post('/login', 'doLogin')->middleware([OnlyGuestMiddleware::class]);
    Route::post('/logout', 'doLogout')->middleware([OnlyMemberMiddleware::class]);
    Route::get('/register', 'register')->middleware([OnlyGuestMiddleware::class]);
    Route::post('/register', 'doRegister')->middleware([OnlyGuestMiddleware::class]);
});

Route::controller(\App\Http\Controllers\CustomerController::class)->middleware([OnlyMemberMiddleware::class])->group(function() {
    Route::get('/customer', 'index');
    Route::get('/customer/create', 'create');
    Route::post('/customer/create', 'doCreate');
    Route::get('/customer/{id}/edit', 'edit');
    Route::post('/customer/{id}/edit', 'doEdit');
    Route::post('/customer/{id}/delete', 'doDelete');
});

Route::controller(\App\Http\Controllers\CustomerCategoryController::class)->middleware([OnlyMemberMiddleware::class])->group(function() {
    Route::get('/customer_category', 'index');
    Route::get('/customer_category/create', 'create');
    Route::post('/customer_category/create', 'doCreate');
    Route::get('/customer_category/{id}/edit', 'edit');
    Route::post('/customer_category/{id}/edit', 'doEdit');
    Route::post('/customer_category/{id}/delete', 'doDelete');
});

Route::controller(\App\Http\Controllers\PaymentController::class)->middleware([OnlyMemberMiddleware::class])->group(function() {
    Route::get('/payment', 'index');
    Route::get('/payment/create', 'create');
    Route::post('/payment/create', 'doCreate');
    Route::get('/payment/{id}/edit', 'edit');
    Route::post('/payment/{id}/edit', 'doEdit');
    Route::post('/payment/{id}/delete', 'doDelete');
    Route::get('/payment/{id}/pay', 'pay');
    Route::post('/payment/{id}/pay', 'doPay');
});