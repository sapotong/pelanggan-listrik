<?php

namespace App\Http\Controllers;

use App\Services\CustomerService;
use App\Services\PaymentService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PaymentController extends Controller
{
    private PaymentService $paymentService;
    private CustomerService $customerService;

    public function __construct(PaymentService $paymentService, CustomerService $customerService)
    {
        $this->paymentService = $paymentService;
        $this->customerService = $customerService;
    }

    public function index(Request $request): Response
    {
        return response()->view('payment.index', [
            'title' => 'Pembayaran',
            'pay_times' => $this->paymentService->getPayTimeByPage()
        ]);
    }

    public function create(Request $request): Response
    {
        return response()->view('payment.create', [
            'title' => 'Tambah Pembayaran'
        ]);
    }

    public function doCreate(Request $request): Response|RedirectResponse
    {
        $request->validate([
            'month' => 'required',
        ]);

        if (!$this->paymentService->createPayTime($request->input('month'))) {
            return response()->view('payment.create', [
                'title' => 'Tambah Pembayaran',
                'error' => 'Tambah pembayaran gagal'
            ]);
        }

        $request->session()->flash('success', 'Tambah pembayaran berhasil');
        return redirect('/payment');
    }

    public function edit(Request $request, int $id): Response|RedirectResponse
    {
        $payment = $this->paymentService->findPayTimeById($id);

        if (!$payment) {
            $request->session()->flash('error', 'Data pembayaran tidak ditemukan');
            return redirect('/payment');
        }

        return response()->view('payment.edit', [
            'title' => 'Edit Pembayaran',
            'payment' => $payment
        ]);
    }

    public function doEdit(Request $request, int $id): RedirectResponse
    {
        $request->validate([
            'month' => 'required',
        ]);

        if (!$this->paymentService->updatePayTimeById($id, $request->input('month'))) {
            return response()->view('payment.create', [
                'title' => 'Edit Pembayaran',
                'error' => 'Edit pembayaran gagal'
            ]);
        }

        $request->session()->flash('success', 'Edit pembayaran berhasil');
        return redirect('/payment');
    }

    public function doDelete(Request $request, int $id): RedirectResponse
    {
        $customer = $this->paymentService->findPayTimeById($id);

        if (!$customer) {
            $request->session()->flash('error', 'Data pembayaran tidak ditemukan');
            return redirect('/payment');
        }

        if (!$this->paymentService->deletePayTimeById($id)) {
            $request->session()->flash('error', 'Hapus pembayaran gagal');
            return redirect('/payment');
        }

        $request->session()->flash('success', 'Hapus pembayaran berhasil');
        return redirect('/payment');
    }

    public function pay(Request $request, int $id): Response
    {
        $payment = $this->paymentService->findPayTimeById($id);
        $customers = $this->paymentService->findAllPayWithCustomerByTime($id);

        return response()->view('payment.pay', [
            'title' => 'Pembayaran',
            'payment' => $payment,
            'customers' => $customers
        ]);
    }

    public function doPay(Request $request, int $id): RedirectResponse
    {
        $request->validate([
            'pays' => 'required',
        ]);

        $items = [];
        foreach($request->input('pays') as $customer_id => $pay) array_push($items, [
            'customer_id' => $customer_id,
            'time_id' => $id,
            'status' => $pay['status'],
            'pay_id' => $pay['pay_id']
        ]);

        if (!$this->paymentService->pay($items)) {
            return response()->view('payment.pay', [
                'title' => 'Pembayaran',
                'error' => 'Pembayaran gagal'
            ]);
        }

        $request->session()->flash('success', 'Pembayaran berhasil');
        return redirect("/payment/{$id}/pay");
    }
}
