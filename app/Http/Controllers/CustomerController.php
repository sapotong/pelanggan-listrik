<?php

namespace App\Http\Controllers;

use App\Services\CustomerService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;

class CustomerController extends Controller
{
    private CustomerService $customerService;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }
    
    public function index(Request $request): Response
    {
        $q = $request->input('q');

        $customers = $this->customerService->findAllByPage($q);
        return response()->view('customer.index', [
            'title' => 'Pelanggan',
            'customers' => $customers
        ]);
    }

    public function create(Request $request): Response
    {
        $customers_categories = $this->customerService->categoryFindAll();

        return response()->view('customer.create', [
            'title' => 'Tambah Pelanggan',
            'customers_categories' => $customers_categories
        ]);
    }

    public function doCreate(Request $request): Response|RedirectResponse
    {
        $request->validate([
            'name' => 'required',
            'number' => 'required',
            'category_id' => 'required',
            'gender' => 'required',
            'dob' => 'required',
            'address' => 'required',
        ]);

        $customer = [
            'name' => $request->input('name'),
            'number' => $request->input('number'),
            'category_id' => $request->input('category_id'),
            'gender' => $request->input('gender'),
            'dob' => $request->input('dob'),
            'address' => $request->input('address'),
        ];

        if (!$this->customerService->create($customer)) {
            return response()->view('customer.create', [
                'title' => 'Tambah Pelanggan',
                'error' => 'Tambah pelanggan gagal'
            ]);
        }

        $request->session()->flash('success', 'Tambah pelanggan berhasil');
        return redirect('/customer');
    }

    public function edit(Request $request, int $id): Response|RedirectResponse
    {
        $customer = $this->customerService->findById($id);

        if (!$customer) {
            $request->session()->flash('error', 'Data pelanggan tidak ditemukan');
            return redirect('/customer');
        }
        $customers_categories = $this->customerService->categoryFindAll();

        return response()->view('customer.edit', [
            'title' => 'Edit Pelanggan',
            'customer' => $customer,
            'customers_categories' => $customers_categories,
        ]);
    }

    public function doEdit(Request $request, int $id)
    {
        $request->validate([
            'name' => 'required',
            'number' => 'required',
            'category_id' => 'required',
            'gender' => 'required',
            'dob' => 'required',
            'address' => 'required',
        ]);

        $customer = [
            'name' => $request->input('name'),
            'number' => $request->input('number'),
            'category_id' => $request->input('category_id'),
            'gender' => $request->input('gender'),
            'dob' => $request->input('dob'),
            'address' => $request->input('address'),
        ];

        if (!$this->customerService->update($id, $customer)) {
            return response()->view('customer.create', [
                'title' => 'Edit Pelanggan',
                'error' => 'Edit pelanggan gagal'
            ]);
        }

        $request->session()->flash('success', 'Edit pelanggan berhasil');
        return redirect('/customer');
    }

    public function doDelete(Request $request, int $id): RedirectResponse
    {
        $customer = $this->customerService->findById($id);

        if (!$customer) {
            $request->session()->flash('error', 'Data pelanggan tidak ditemukan');
            return redirect('/customer');
        }

        if (!$this->customerService->delete($id)) {
            $request->session()->flash('error', 'Hapus pelanggan gagal');
            return redirect('/customer');
        }

        $request->session()->flash('success', 'Hapus pelanggan berhasil');
        return redirect('/customer');
    }
}
