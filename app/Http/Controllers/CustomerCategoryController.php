<?php

namespace App\Http\Controllers;

use App\Services\CustomerService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CustomerCategoryController extends Controller
{
    private CustomerService $customerService;

    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }
    public function index(Request $request): Response
    {
        $q = $request->input('q');

        $customers_categories = $this->customerService->categoryFindAllByPage($q);
        return response()->view('customer_category.index', [
            'title' => 'Kategori Pelanggan',
            'customers_categories' => $customers_categories
        ]);
    }

    public function create(Request $request): Response
    {
        return response()->view('customer_category.create', [
            'title' => 'Tambah Kategori Pelanggan'
        ]);
    }

    public function doCreate(Request $request): Response|RedirectResponse
    {
        $request->validate([
            'name' => 'required',
        ]);

        $customer_category = [
            'name' => $request->input('name'),
        ];

        if (!$this->customerService->categoryCreate($customer_category)) {
            return response()->view('customer_category.create', [
                'title' => 'Tambah Kategori Pelanggan',
                'error' => 'Tambah kategori pelanggan gagal'
            ]);
        }

        $request->session()->flash('success', 'Tambah kategori pelanggan berhasil');
        return redirect('/customer_category');
    }

    public function edit(Request $request, int $id): Response|RedirectResponse
    {
        $customer_category = $this->customerService->categoryFindById($id);

        if (!$customer_category) {
            $request->session()->flash('error', 'Data kategori pelanggan tidak ditemukan');
            return redirect('/customer_category');
        }

        return response()->view('customer_category.edit', [
            'title' => 'Edit Kategori Pelanggan',
            'customer_category' => $customer_category
        ]);
    }

    public function doEdit(Request $request, int $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $customer_category = [
            'name' => $request->input('name'),
        ];

        if (!$this->customerService->categoryUpdate($id, $customer_category)) {
            return response()->view('customer_category.create', [
                'title' => 'Edit Kategori Pelanggan',
                'error' => 'Edit kategori pelanggan gagal'
            ]);
        }

        $request->session()->flash('success', 'Edit kategori pelanggan berhasil');
        return redirect('/customer_category');
    }

    public function doDelete(Request $request, int $id): RedirectResponse
    {
        $customer = $this->customerService->categoryFindById($id);

        if (!$customer) {
            $request->session()->flash('error', 'Data kategori pelanggan tidak ditemukan');
            return redirect('/customer_category');
        }

        if (!$this->customerService->categoryDelete($id)) {
            $request->session()->flash('error', 'Hapus kategori pelanggan gagal');
            return redirect('/customer_category');
        }

        $request->session()->flash('success', 'Hapus kategori pelanggan berhasil');
        return redirect('/customer_category');
    }
}
