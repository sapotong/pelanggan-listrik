<?php

namespace App\Services;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface CustomerService {
    public function create(array $customer): bool|int;
    public function update(int $id, array $customer): bool;
    public function delete(int $id): bool;

    public function findAllByPage(?string $q, int $perpage = 10): LengthAwarePaginator;
    public function findById(int $id): object|null;
    public function findAll(): Collection;
    
    public function categoryCreate(array $customerCatgeory): bool|int;
    public function categoryUpdate(int $id, array $customerCatgeory): bool;
    public function categoryDelete(int $id): bool;

    public function categoryFindAllByPage(?string $q, int $perpage = 10): LengthAwarePaginator;
    public function categoryFindById(int $id): object|null;
    public function categoryFindAll(): array;

}