<?php

namespace App\Services\Impl;

use App\Services\CustomerService;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CustomerServiceImpl implements CustomerService
{
    public function create(array $customer): int
    {
        $customer['created_at'] = date('Y-m-d H:i:s');
        return DB::table('customers')->insertGetId($customer);
    }

    public function update(int $id, array $customer): bool
    {
        $customer['updated_at'] = date('Y-m-d H:i:s');
        return DB::table('customers')->where('id', $id)->update($customer);
    }

    public function delete(int $id): bool
    {
        return DB::table('customers')->where('id', $id)->delete();
    }

    public function findAllByPage(?string $q, int $perpage = 10): LengthAwarePaginator
    {
        $paginator = DB::table('customers')
            ->join('customers_categories', 'customers.category_id', '=', 'customers_categories.id')
            ->select('customers.*', 'customers_categories.name AS category')
            ->where('customers.name', 'LIKE', "%{$q}%")
            ->paginate($perpage);

        $genders = ['1' => 'Laki-laki', '2' => 'Perempuan'];
        foreach($paginator->items() as $item) {
            $item->gender = $genders[$item->gender];
        }

        return $paginator;
    }
    
    public function findAll(): Collection
    {
        $data = DB::table('customers')
            ->join('customers_categories', 'customers.category_id', '=', 'customers_categories.id')
            ->select('customers.*', 'customers_categories.name AS category')
            ->get();

        return $data;
    }

    public function findById(int $id): object|null
    {
        return DB::table('customers')->find($id);
    }

    public function categoryCreate(array $customerCategory): int
    {
        $customerCategory['created_at'] = date('Y-m-d H:i:s');

        return DB::table('customers_categories')->insertGetId($customerCategory);
    }

    public function categoryUpdate(int $id, array $customerCategory): bool
    {
        $customerCategory['updated_at'] = date('Y-m-d H:i:s');
        return DB::table('customers_categories')->where('id', $id)->update($customerCategory);
    }

    public function categoryDelete(int $id): bool
    {
        return DB::table('customers_categories')->where('id', $id)->delete();
    }

    public function categoryFindAllByPage(?string $q, int $perpage = 10): LengthAwarePaginator
    {
        return DB::table('customers_categories')
            ->where('name', 'LIKE', "%{$q}%")
            ->paginate($perpage);
    }

    public function categoryFindById(int $id): object|null
    {
        return DB::table('customers_categories')->find($id);
    }

    public function categoryFindAll(): array
    {
        return DB::table('customers_categories')->get()->toArray();
    }
}