<?php

namespace App\Services\Impl;

use App\Services\PaymentService;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class PaymentServiceImpl implements PaymentService
{
    public function createPayTime(string $month): bool
    {
        return DB::table('payments_times')->insert(['month' => $month, 'created_at' => date('Y-m-d H:i:s')]);
    }

    public function getPayTimeByPage(int $perpage = 10): LengthAwarePaginator
    {
        $paginator = DB::table('payments_times')
            ->paginate($perpage);

        return $paginator;
    }

    public function findPayTimeById(int $id): object
    {
        return DB::table('payments_times')->find($id);
    }
    
    public function deletePayTimeById(int $id): bool
    {
        return DB::table('payments_times')->delete($id);
    }

    public function updatePayTimeById(int $id, string $month): bool
    {
        $data = [
            'updated_at' => date('Y-m-d H:i:s'),
            'month' => $month
        ];
        
        return DB::table('payments_times')->where('id', $id)->update($data);
    }

    public function pay(array $items): bool
    {
        DB::beginTransaction();
        try {
            foreach ($items as $item) {
                if (empty($item['pay_id'])) DB::table('payments')->insert([
                    'customer_id' => $item['customer_id'],
                    'time_id' => $item['time_id'],
                    'status' => $item['status'],
                    'created_at' => date('Y-m-d H:i:s')
                ]);
                else DB::table('payments')->where('id', '=', $item['pay_id'])->update([
                    'customer_id' => $item['customer_id'],
                    'time_id' => $item['time_id'],
                    'status' => $item['status'],
                    'updated_at' => date('Y-m-d H:i:s')
                ]); 
            }

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            return false;
        }
    }

    public function findAllPayWithCustomerByTime(int $id): Collection
    {
        return DB::table('customers')
            ->leftJoin('payments', function ($join) use ($id) {
                $join->on('payments.customer_id', '=', 'customers.id')
                    ->where('payments.time_id', '=', $id);
            })
            ->select('customers.*', 'payments.status', 'payments.id as pay_id')->get();
    }
}
