<?php

namespace App\Services;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface PaymentService {
    public function createPayTime(string $month): bool;
    public function updatePayTimeById(int $id, string $month): bool;
    public function getPayTimeByPage(int $perpage = 10): LengthAwarePaginator;
    public function findPayTimeById(int $id): object;
    public function deletePayTimeById(int $id): bool;

    public function pay(array $item): bool;
    public function findAllPayWithCustomerByTime(int $id): Collection;
}