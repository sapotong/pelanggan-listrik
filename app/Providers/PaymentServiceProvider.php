<?php

namespace App\Providers;

use App\Services\Impl\PaymentServiceImpl;
use App\Services\PaymentService;
use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
    public array $singletons = [
        PaymentService::class => PaymentServiceImpl::class
    ];

    public function provides() : array
    {
        return [PaymentService::class];
    }
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
